#!/bin/bash
tinygo build -scheduler=none --no-debug \
    -o hello-service.wasm \
    -target wasi main.go

ls -lh *.wasm
