#!/bin/bash
operating_system="linux"
processor="amd64"

DOCKER_USER="botsgarden"
IMAGE_NAME="slingshot-${operating_system}-${processor}"
IMAGE_TAG="0.0.1"
echo "🖼️ ${IMAGE_NAME}"
HTTP_PORT="8080"

docker run \
  -p ${HTTP_PORT}:${HTTP_PORT} \
  -v $(pwd):/app --rm ${DOCKER_USER}/${IMAGE_NAME}:${IMAGE_TAG} \
  /slingshot start \
  --wasm=./app/hello-service.wasm \
  --handler=handle \
  --http-port=${HTTP_PORT} 


