# slingshot-on-fly-io

## What?

This application is based on my side project: [SlingShot](https://bots-garden.github.io/slingshot/). This is a small GoLang application server.

## Requirements

- Create an account on fly.io
- Create a fly.io token
  - Use https://fly.io/user/personal_access_tokens
  - Copy the generated value
  - Create an environment variable `FLY_ACCESS_TOKEN` (with the token value) at the group level or project level 
